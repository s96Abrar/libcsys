#-------------------------------------------------
#
# Project created by QtCreator 2018-06-20T09:12:54
#
#-------------------------------------------------

QT      += core network dbus
QT      -= gui

TARGET   = csys
TEMPLATE = lib

VERSION  = 4.1.0

# thread - Enable threading support
# silent - Do not print the compilation syntax
CONFIG  += thread silent

# Disable QDebug on Release build
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Warn about deprecated features
DEFINES += QT_DEPRECATED_WARNINGS
# Disable all deprecated features before Qt 5.15
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x051500

# Definetion section
DEFINES += LIBCSYS_LIBRARY

INCLUDEPATH += ./csys/
DEPENDPATH  += ./csys/

# Build section
MOC_DIR			= build/moc
OBJECTS_DIR		= build/obj
RCC_DIR			= build/qrc
UI_DIR			= build/uic

# C++17 Support for Qt5
QMAKE_CXXFLAGS += -std=c++17

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        INSTALLS	+= target includes
        CONFIG		+= create_pc create_prl no_install_prl link_pkgconfig
        contains(DEFINES, LIB64): target.path = $$PREFIX/lib64
        else: target.path = $$PREFIX/lib

        includes.files              = libcsys_global.h csys/*.h
        includes.path               = $$PREFIX/include/csys

        QMAKE_PKGCONFIG_FILE        = csys
        QMAKE_PKGCONFIG_NAME        = libcsys
        QMAKE_PKGCONFIG_DESCRIPTION = Library for getting system resource information in real time.
        QMAKE_PKGCONFIG_PREFIX      = $$PREFIX
        QMAKE_PKGCONFIG_LIBDIR      = $$target.path
        QMAKE_PKGCONFIG_INCDIR      = $$includes.path
        QMAKE_PKGCONFIG_VERSION     = $$VERSION
        QMAKE_PKGCONFIG_DESTDIR     = pkgconfig # Destination Directory where pkgconfig installed
}

HEADERS += \
    csys/cpu_info.h \
    csys/disk_info.h \
    csys/memory_info.h \
    csys/network_info.h \
    csys/system_info.h \
    csys/file_util.h \
    csys/battery.h \
    csys/info_manager.h \
    csys/power.h \
    csys/storageinfo.h \
    libcsys_global.h

SOURCES += \
    csys/cpu_info.cpp \
    csys/disk_info.cpp \
    csys/memory_info.cpp \
    csys/network_info.cpp \
    csys/system_info.cpp \
    csys/file_util.cpp \
    csys/battery.cpp \
    csys/info_manager.cpp \
    csys/power.cpp \
    csys/storageinfo.cpp \
