/*
    *
    * This file is a part of Libcsys.
    * Library for getting system resource information in real time.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QFile>
#include <math.h>
#include <unistd.h>

#include "cpu_info.h"
#include "file_util.h"


CpuInfo *CpuInfo::info = nullptr;

CpuInfo *CpuInfo::instance()
{
    if (not info) {
		info = new CpuInfo();
    }

    return info;
}

QStringList CpuInfo::getCPUFrequenciesStr() const
{
    return mCpuFreqsStr;
}

QList<qreal> CpuInfo::getCPUFrequencies() const
{
    return mCpuFreqs;
}

quint8 CpuInfo::getCpuCoreCount() const
{
    return qMax(1, (int)sysconf(_SC_NPROCESSORS_ONLN));
};

QList<double> CpuInfo::getLoadAvgs() const
{
    return mLoadAvgs;
}

QString CpuInfo::formatKHz(double value) const
{
    if (value >= 1000000) {
        return QString::number(value / 1000000.0) + " GHz";
    } else if (value >= 1000) {
        return QString::number(value / 1000.0) + " MHz";
    } else {
        return QString::number(value) + " KHz";
    }
}

QList<int> CpuInfo::getCpuPercents() const
{
	return mCpuPercents;
}

CpuInfo::CpuInfo() : QObject()
{
    readTimer.start(1000, this);

    QStringList lastReadList = FileUtil::readListFromFile(PROC_STAT);
	lastReadList.removeFirst();

    for (int i = 0; i < getCpuCoreCount(); i++) {
        lastRead[ i ] = CpuTimes(lastReadList.value(i));
    }
};

void CpuInfo::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == readTimer.timerId()) {

        // Read the load averages
        QStringList strListAvgs = FileUtil::readStringFromFile(PROC_LOADAVG).split(QRegExp("\\s+"));

        if (strListAvgs.count() > 2) {
			mLoadAvgs.clear();
			mLoadAvgs << strListAvgs.takeFirst().toDouble();
			mLoadAvgs << strListAvgs.takeFirst().toDouble();
			mLoadAvgs << strListAvgs.takeFirst().toDouble();
		}

        // Read and compute the CPU usage percentages
		mCpuPercents.clear();
        QStringList times = FileUtil::readListFromFile(PROC_STAT);

		/* First is the average of all the cores */
		times.removeFirst();

		/* Current values */
        for (int i = 0; i < getCpuCoreCount(); i++) {
            thisRead[ i ] = CpuTimes(times.value(i));

			qreal totalD = thisRead[ i ].total() - lastRead[ i ].total();
			qreal idleD = thisRead[ i ].idle() - lastRead[ i ].idle();

            mCpuPercents << 100 * (totalD - idleD) / totalD;
		}

		lastRead = thisRead;
		thisRead.clear();

        // Read cpu frequencies
        mCpuFreqs.clear();
        mCpuFreqsStr.clear();

        QFile mSourceFile(PROC_CPUINFO);

        if (mSourceFile.open(QIODevice::ReadOnly)) {
            QTextStream textStream(&mSourceFile);
            QStringList lines = textStream.readAll().split('\n');
            mSourceFile.close();
            QString cpuID = "";

            Q_FOREACH (QString str, lines) {
                if (str.startsWith("processor")) {
                    cpuID = "cpu" + str.section(':', 1, 1).trimmed();
                    continue;
                }

                if (str.startsWith("cpu MHz")) {
                    str = str.section(':', 1, 1).trimmed();
                    double val = str.toDouble();
                    uint kHz = floor(val * pow(10, -2) + .5) * pow(10, 2);
                    str = formatKHz(kHz * 1000);

                    mCpuFreqs.append(val);
                    mCpuFreqsStr.append(cpuID + ": " + str);
                }
            }
        }

		tEvent->accept();
		return;
	}

    QObject::timerEvent(tEvent);
};

CpuTimes::CpuTimes()
{
    mTotal = 0;
    mIdle = 0;
}

CpuTimes::CpuTimes(QString cpuTimes)
{
    /*  user nice system idle iowait  irq  softirq steal guest guest_nice
       cpu  4705 356  584    3699   23    23     0       0     0      0         <<=== Remove this line and use
        .
       cpuN 4705 356  584    3699   23    23     0       0     0      0

         The meanings of the columns are as follows, from left to right:
            - user: normal processes executing in user mode
            - nice: niced processes executing in user mode
            - system: processes executing in kernel mode
            - idle: twiddling thumbs
            - iowait: waiting for I/O to complete
            - irq: servicing interrupts
            - softirq: servicing softirqs
            - steal: involuntary wait
            - guest: running a normal guest
            - guest_nice: running a niced guest
       */

    QList<qreal> times;
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	QStringList timesList = cpuTimes.trimmed().split(QRegExp("\\s"), Qt::SkipEmptyParts);
#else
	QStringList timesList = cpuTimes.trimmed().split(QRegExp("\\s"), QString::SkipEmptyParts);
#endif

    for (int i = 1; i < timesList.count(); i++) {
        times << timesList.at(i).toDouble();
    }

    if (times.count() >= 8) {
        mTotal = times[ 0 ] + times[ 1 ] + times[ 2 ] + times[ 3 ] + times[ 4 ] + times[ 5 ] + times[ 6 ] + times[ 7 ];
        mIdle = times[ 3 ] + times[ 4 ];
    }
}

qreal CpuTimes::total()
{
    return mTotal;
}

qreal CpuTimes::idle()
{
    return mIdle;
}
