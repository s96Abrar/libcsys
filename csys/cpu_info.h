/*

    * This file is a part of Libcsys.
    * Library for getting system resource information in real time.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include "libcsys_global.h"

const QString PROC_CPUINFO = "/proc/cpuinfo";
const QString PROC_LOADAVG = "/proc/loadavg";
const QString PROC_STAT    = "/proc/stat";

class CpuTimes {

public:
    CpuTimes();
    CpuTimes(QString cpuTimes);
    qreal total();
    qreal idle();

private:
    qreal mTotal = 0;
    qreal mIdle = 0;
};

class LIBCSYSSHARED_EXPORT CpuInfo : public QObject {
	Q_OBJECT

public:
    static CpuInfo *instance();

    QStringList getCPUFrequenciesStr() const;
    QList<qreal> getCPUFrequencies() const;
    quint8 getCpuCoreCount() const;
    QList<int> getCpuPercents() const;
    QList<double> getLoadAvgs() const;

    QString formatKHz(double value) const;

private:
    /* Init */
    CpuInfo();

    static CpuInfo *info;

    QStringList mCpuFreqsStr;
    QList<qreal> mCpuFreqs;
    QList<int> mCpuPercents;
    QList<double> mLoadAvgs = { 0, 0, 0 };

    QBasicTimer readTimer;

    QHash<int, CpuTimes> lastRead;
    QHash<int, CpuTimes> thisRead;

protected:
    void timerEvent(QTimerEvent *tEvent);
};
