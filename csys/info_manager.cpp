/*

    * This file is a part of Libcsys.
    * Library for getting system resource information in real time.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include "info_manager.h"

#include "cpu_info.h"
#include "disk_info.h"
#include "memory_info.h"
#include "network_info.h"
#include "system_info.h"


InfoManager *InfoManager::instance = nullptr;

InfoManager *InfoManager::ins()
{
    if ( ! instance ) {
        instance = new InfoManager;
    }

    return instance;
}

InfoManager::InfoManager()
{
    ci = CpuInfo::instance();
    di = new DiskInfo;
    mi = new MemoryInfo;
    ni = NetworkInfo::instance();
    si = new SystemInfo;
}

/*
 * CPU Provider
 */
quint8 InfoManager::getCpuCoreCount() const
{
    return ci->getCpuCoreCount();
}

QList<int> InfoManager::getCpuPercents() const
{
    return ci->getCpuPercents();
}

QList<double> InfoManager::getCpuLoadAvgs() const
{
    return ci->getLoadAvgs();
}

QStringList InfoManager::getCpuFrequenciesStr() const
{
    return ci->getCPUFrequenciesStr();
}

/*
 * Memory Provider
 */
void InfoManager::updateMemoryInfo()
{
    mi->updateMemoryInfo();
}

quint64 InfoManager::getSwapUsed() const
{
    return mi->getSwapUsed();
}

quint64 InfoManager::getSwapTotal() const
{
    return mi->getSwapTotal();
}

quint64 InfoManager::getMemUsed() const
{
    return mi->getMemUsed();
}

quint64 InfoManager::getMemTotal() const
{
    return mi->getMemTotal();
}

/*
 * Disk Provider
 */
QList<Disk *> InfoManager::getDisks() const
{
    return di->getDisks();
}

void InfoManager::updateDiskInfo()
{
    di->updateDiskInfo();
}

QList<quint64> InfoManager::getDiskIO()
{
    return di->getDiskIO();
}

/*
 * Network Provider
 */
QList<quint64> InfoManager::getNetworkIO() const
{
    return ni->getIOBytes();
}

/*
 * System Provider
 */
QFileInfoList InfoManager::getCrashReports() const
{
    return si->getCrashReports();
}

QFileInfoList InfoManager::getAppLogs() const
{
    return si->getAppLogs();
}

QFileInfoList InfoManager::getAppCaches() const
{
    return si->getAppCaches();
}

QString InfoManager::getUserName() const
{
    return si->getUsername();
}
