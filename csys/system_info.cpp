/*

    * This file is a part of Libcsys.
    * Library for getting system resource information in real time.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QStandardPaths>
#include <QFileInfo>
#include <QProcess>
#include <QDir>

#include <unistd.h>

#include "system_info.h"
#include "file_util.h"
#include "cpu_info.h"


SystemInfo::SystemInfo()
{
	QProcess proc;
	proc.start("lscpu", QStringList());
	proc.waitForFinished();

	cpuModel = "unknown";
	cpuSpeed = "unknown";

    QStringList lines = QString::fromLocal8Bit(proc.readAll()).split("\n");

    Q_FOREACH (QString line, lines) {
        if (line.trimmed().simplified().startsWith("Model name:")) {
            cpuModel = line.split(": ").value(1).trimmed();
        }

        if (line.trimmed().simplified().startsWith("CPU max MHz:")) {
            cpuSpeed = line.split(": ").value(1).trimmed().split(".").value(0) + " MHz";
        }
    }


    cpuCore = QString::number(CpuInfo::instance()->getCpuCoreCount());

    // get username
    QString name = qgetenv("USER");

    if (name.isEmpty()) {
        name = qgetenv("USERNAME");
    }

    try {
        if (name.isEmpty()) {
			proc.start("whoami", QStringList());
            proc.waitForFinished();
            name = QString::fromLocal8Bit(proc.readAllStandardOutput());
        }
    } catch (const QString &ex) {
        qCritical() << ex;
	}

    this->username = name;
}

QString SystemInfo::getUsername() const
{
    return username;
}

QString SystemInfo::getHostname() const
{
    char *hostname = new char[ 256 ];
    int size = gethostname(hostname, 256);

    return QString::fromLatin1(hostname, size);
}

QString SystemInfo::getPlatform() const
{
    return QString("%1 %2")
           .arg(QSysInfo::kernelType())
           .arg(QSysInfo::currentCpuArchitecture());
}

QString SystemInfo::getDistribution() const
{
    return QSysInfo::prettyProductName();
}

QString SystemInfo::getKernel() const
{
    return QSysInfo::kernelVersion();
}

QString SystemInfo::getCpuModel() const
{
    return this->cpuModel;
}

QString SystemInfo::getCpuSpeed() const
{
    return this->cpuSpeed;
}

QString SystemInfo::getCpuCore() const
{
    return this->cpuCore;
}

QString SystemInfo::getBootTime() const
{
	QString uptime = "unknown";
    QProcess proc;
	proc.start("systemd-analyze", QStringList());
    proc.waitForFinished();
    QString rawTime = QString::fromLocal8Bit(proc.readAll());

    if (not rawTime.isEmpty()) {
        uptime = rawTime.split("\n").value(0).split("=").value(1).trimmed();
	}

	return uptime;
}

QString SystemInfo::getUptime() const
{
    QString rawTime = FileUtil::readStringFromFile("/proc/uptime").trimmed();
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	double uptime = rawTime.split(" ", Qt::SkipEmptyParts).value(0).toDouble();
#else
	double uptime = rawTime.split(" ", QString::SkipEmptyParts).value(0).toDouble();
#endif

    int h = (int)(uptime / 3600);
    int m = (int)(uptime / 60 - h * 60);
    int s = (int)(uptime - h * 3600 - m * 60);

	return QString("%1h %2m %3s").arg(h).arg(m).arg(s);
}

QFileInfoList SystemInfo::getCrashReports() const
{
    QDir reports("/var/crash");

    return reports.entryInfoList(QDir::Files);
}

QFileInfoList SystemInfo::getAppLogs() const
{
    QDir logs("/var/log");

    return logs.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
}

QFileInfoList SystemInfo::getAppCaches() const
{
    QString homePath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QDir caches(homePath + "/.cache");

    return caches.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
}
