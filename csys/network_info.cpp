/*

    * This file is a part of Libcsys.
    * Library for getting system resource information in real time.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QNetworkInterface>

#include "network_info.h"
#include "file_util.h"

NetworkInfo *NetworkInfo::netInfo = nullptr;

NetworkInfo *NetworkInfo::instance()
{
    if (!netInfo) {
        netInfo = new NetworkInfo;
    }

    return netInfo;
}

NetworkInfo::NetworkInfo() : netDataPath( "/proc/net/dev" )
{
}

QList<quint64> NetworkInfo::getIOBytes() const
{
    QList<quint64> iobytes;

    iobytes << 0 << 0;

    QStringList data = FileUtil::readListFromFile( netDataPath, QIODevice::ReadOnly );

    // Remove the first three lines
    data.removeFirst();
    data.removeFirst();
    data.removeFirst();

    for( QString line: data ) {
        QStringList bytes = line.trimmed().split( QRegExp( "\\s+" ) );
        iobytes[ 0 ] += bytes.at( 1 ).toULongLong();
        iobytes[ 1 ] += bytes.at( 9 ).toULongLong();
    }

    return iobytes;
}
