/*

    * This file is a part of Libcsys.
    * Library for getting system resource information in real time.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include <QFileInfoList>

#include "libcsys_global.h"

class Disk;
class Process;
class CpuInfo;
class DiskInfo;
class MemoryInfo;
class NetworkInfo;
class SystemInfo;
class ProcessInfo;

class LIBCSYSSHARED_EXPORT InfoManager {

public:
    static InfoManager *ins();

    InfoManager();

    quint8 getCpuCoreCount() const;
    QList<int> getCpuPercents() const;
    QList<double> getCpuLoadAvgs() const;
    QStringList getCpuFrequenciesStr() const;

    quint64 getSwapUsed() const;
    quint64 getSwapTotal() const;
    quint64 getMemUsed() const;
    quint64 getMemTotal() const;
    void updateMemoryInfo();

    QList<quint64> getNetworkIO() const;

    QList<Disk *> getDisks() const;
    QList<quint64> getDiskIO();
    void updateDiskInfo();

    QFileInfoList getCrashReports() const;
    QFileInfoList getAppLogs() const;
    QFileInfoList getAppCaches() const;
    QString getUserName() const;

private:
    CpuInfo *ci = nullptr;
    DiskInfo *di = nullptr;
    MemoryInfo *mi = nullptr;
    NetworkInfo *ni = nullptr;
    SystemInfo *si = nullptr;
    ProcessInfo *pi = nullptr;

    static InfoManager *instance;

};
